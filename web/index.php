<?php

// Front controller

use Symfony\Component\HttpFoundation\Request;

require '../vendor/autoload.php';

$request = Request::createFromGlobals();

$blapi = new \Blapi\BlapiKernel();

$response = $blapi->handle($request);
$response->send();

$blapi->terminate($request, $response);
