# blapi

blapi is a blogging platform created with frontend developers in mind. It shares convenient, designed with UX in mind admin panel focused on productivity during writing content and flexible Web API for use with frontend frameworks.

## Installation

1. Run `npm install` to install npm dependencies.
2. Run `gulp` to compile scripts to public folder.