<?php

namespace Blapi\Twig;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Twig_Extension;

class TwigExtension extends Twig_Extension
{
    /**
     * @var UrlGeneratorInterface
     */
    private $urlGenerator;

    public function __construct(UrlGeneratorInterface $urlGenerator)
    {
        $this->urlGenerator = $urlGenerator;
    }

    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('route', [$this, 'route']),
            new \Twig_SimpleFunction('asset', [$this, 'asset'])
        ];
    }

    public function route($routeName, $parameters = [])
    {
        return $this->urlGenerator->generate($routeName, $parameters);
    }

    public function asset($path)
    {
        return $this->urlGenerator->getContext()->getBaseUrl() . '/assets/' . $path;
    }
}