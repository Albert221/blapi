<?php

namespace Blapi;

use Blapi\Controller\ControllerResolver;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader as ContainerYamlFileLoader;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Controller\ArgumentResolver;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\HttpKernel\EventListener\RouterListener;
use Symfony\Component\HttpKernel\HttpKernel;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Symfony\Component\Routing\Loader\YamlFileLoader as RoutingYamlFileLoader;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\RouteCollection;

class BlapiKernel extends HttpKernel
{
    /**
     * @var ContainerBuilder
     */
    private $container;

    /**
     * Blapi constructor.
     */
    public function __construct()
    {
        $this->initializeContainer();
        $this->initializeRouting();

        /** @var RouteCollection $routes */
        $routes = $this->container->get('blapi.routes');

        /** @var RequestContext $requestContext */
        $requestContext = $this->container->get('blapi.requestContext');
        $matcher = new UrlMatcher($routes, $requestContext);

        $this->container->set('blapi.urlGenerator', new UrlGenerator($routes, $requestContext));

        // FIXME: Make this a separate method
        $dispatcher = new EventDispatcher();
        $dispatcher->addSubscriber(new RouterListener($matcher, new RequestStack()));
        $dispatcher->addListener(KernelEvents::VIEW, function (GetResponseForControllerResultEvent $e) {
            $e->setResponse(new Response($e->getControllerResult()));
        });

        $controllerResolver = new ControllerResolver($this->container);
        $argumentResolver = new ArgumentResolver();

        parent::__construct($dispatcher, $controllerResolver, new RequestStack(), $argumentResolver);
    }

    private function initializeContainer()
    {
        $this->container = new ContainerBuilder();
        $loader = new ContainerYamlFileLoader($this->container, new FileLocator(dirname(__DIR__).'/config'));
        $loader->load('services.yml');

        $this->container->set('blapi.kernel', $this);
        $this->container->setParameter('blapi.root_dir', dirname(__DIR__));
    }

    private function initializeRouting()
    {
        $loader = new RoutingYamlFileLoader(new FileLocator(dirname(__DIR__).'/config'));
        $routes = $loader->load('routes.yml');
        
        $this->container->set('blapi.routes', $routes);
    }
}