<?php

namespace Blapi\Controller;

class PostController extends BaseController
{
    public function all()
    {
        return $this->view('posts/all.twig');
    }

    public function add()
    {
        return $this->view('posts/add.twig');
    }
}