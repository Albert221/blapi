<?php

namespace Blapi\Controller;

use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Controller\ControllerResolver as BaseControllerResolver;

class ControllerResolver extends BaseControllerResolver
{
    protected $container;

    /**
     * {@inheritdoc}
     */
    public function __construct(ContainerInterface $container, LoggerInterface $logger = null)
    {
        $this->container = $container;

        parent::__construct($logger);
    }

    /**
     * {@inheritdoc}
     */
    protected function instantiateController($class)
    {
        if ($this->container->has($class)) {
            $controller = $this->container->get($class);
        } else {
            $controller = parent::instantiateController($class);
        }

        if ($controller instanceof BaseController) {
            $controller->setTwig($this->container->get('twig'));
        }

        return $controller;
    }
}