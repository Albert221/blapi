<?php

namespace Blapi\Controller;

use Twig_Environment;

abstract class BaseController
{
    /**
     * @var Twig_Environment
     */
    protected $twig;

    public function setTwig(Twig_Environment $twig)
    {
        $this->twig = $twig;
    }

    protected function view($name, $data = [])
    {
        return $this->twig->render($name, $data);
    }
}